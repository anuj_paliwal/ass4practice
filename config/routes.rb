Links::Application.routes.draw do
	resources :users
	resources :links do
		resources :comments, :only => [:destroy]
	end
	resource :session
	resources :comments, :except => [:destroy]
end
