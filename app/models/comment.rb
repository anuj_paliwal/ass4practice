class Comment < ActiveRecord::Base
  attr_accessible :body, :link_id
  validates :body, :presence => true

end
