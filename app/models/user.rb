class User < ActiveRecord::Base
  attr_accessible :username, :password, :session_token
  validates :password, :username, :presence => true
  validates :password, :length => { :minimum => 6 }

  def reset_token
  	self.session_token = SecureRandom::urlsafe_base64(16)
  	self.save!
  	self.session_token
  end
  
end
