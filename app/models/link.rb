class Link < ActiveRecord::Base
  attr_accessible :user_id, :title, :url
  validates :title, :presence => true
  validates :url, :presence => { :message => "Url can't be blank"}

  has_many(
  	:comments,
  	:class_name => "Comment",
  	:foreign_key => :link_id,
  	:primary_key => :id
  )
end
