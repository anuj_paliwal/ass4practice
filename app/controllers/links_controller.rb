class LinksController < ApplicationController

	def index
		redirect_to new_session_url unless logged_in?
		@users = User.all
		@links = Link.all
	end

	def new
		redirect_to new_session_url unless logged_in?
	end

	def create
		@link = Link.new(params[:link])
		if @link.save
			#session[:session_token] = @user.reset_token
			#fail
			redirect_to link_url(@link)
		else
			flash[:errors] = @link.errors.full_messages
			redirect_to new_link_url
		end
	end

	def show
		@link = Link.find(params[:id])
	end

	def edit
		@link = Link.find(params[:id])
	end

	def update
		@link = Link.find(params[:id])
		@link.update_attributes(params[:link])
		if @link.save
			redirect_to link_url(@link)
		else
			flash[:errors] = @link.errors.full_messages
			redirect_to link_url(@link)
		end
	end

end
