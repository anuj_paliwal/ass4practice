class CommentsController < ApplicationController

	def new
	end

	def create
		@comment = Comment.new(params[:comment])
		if @comment.save
			redirect_to link_url(@comment[:link_id])
		else
			flash[:errors] = @comment.errors.full_messages
			redirect_to link_url(@comment[:link_id])
		end

	end

	def destroy
		@comment = Comment.find(params[:link_id])
		@comment.destroy
		redirect_to link_url(params[:id])

	end

end
