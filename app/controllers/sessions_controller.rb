class SessionsController < ApplicationController

	def new
	end

	def create
		@user = User.find_by_username_and_password(
			params[:user][:username],
			params[:user][:password]
		)
		if @user
			session[:session_token] = @user.reset_token
			redirect_to links_url
		else
			flash[:errors] = ["Invalid Username or Password"]
			redirect_to new_session_url
		end

	end

	def destroy
		session[:session_token] = nil
		self.current_user = nil
		redirect_to new_session_url
	end

end
