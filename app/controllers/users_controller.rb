class UsersController < ApplicationController

	def new
	end

	def create
		@user = User.new(params[:user])
		if @user.save
			session[:session_token] = @user.reset_token
			#fail
			redirect_to links_url
		else
			flash[:errors] = @user.errors.full_messages
			redirect_to new_user_url
		end
	end


	#links index

end
