module SessionsHelper
	attr_writer :current_user

	def current_user
		return nil unless session[:session_token]
		@current_user ||= User.find_by_session_token(session[:session_token])
		@current_user
	end

	def logged_in?
		!!self.current_user
	end

end
